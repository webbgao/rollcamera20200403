/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_facebook_login_widget_FacebookJNIUTil */

#ifndef _Included_com_facebook_login_widget_FacebookJNIUTil
#define _Included_com_facebook_login_widget_FacebookJNIUTil
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    mg
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_mg
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    yd
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_yd
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    cj
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_cj
  (JNIEnv *, jclass);


  /*
   * Class:     com_facebook_login_widget_FacebookJNIUTil
   * Method:    cj
   * Signature: ()Ljava/lang/String;
   */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_flb
    (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    g
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_g
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    p
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_p
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f0
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f0
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f1
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f1
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f2
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f2
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f3
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f3
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f4
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f4
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f5
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f5
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f6
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f6
  (JNIEnv *, jclass);

/*
 * Class:     com_facebook_login_widget_FacebookJNIUTil
 * Method:    f7
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_f7
  (JNIEnv *, jclass);


JNIEXPORT jstring JNICALL Java_com_facebook_login_widget_FacebookJNIUTil_m
  (JNIEnv *, jclass);

#ifdef __cplusplus
}
#endif
#endif
