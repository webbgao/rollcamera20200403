package com.bdeki.rollcamera;

import android.app.Application;

import com.facebook.login.widget.Facebook;
import com.kochava.base.Tracker;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Tracker.configure(new Tracker.Configuration(getApplicationContext())
                .setAppGuid("koroll-camera-1srbf")
        );
        UMConfigure.init(this, UMConfigure.DEVICE_TYPE_PHONE, null);
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
        Facebook.init(this);
    }
}
