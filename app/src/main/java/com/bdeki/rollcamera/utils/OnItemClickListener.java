package com.bdeki.rollcamera.utils;

import android.view.View;

/**
 * Click
 */
public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
