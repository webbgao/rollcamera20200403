package com.bdeki.rollcamera;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.facebook.login.widget.FacebookJNIUTil;
import com.facebook.login.widget.FacebookUtil;

public class SplashActivity extends AppCompatActivity {
    //htJFIDFtps://JFIDFbuckJFIDFet-rollcaera.sJFIDF3.ap-souJFIDFtheasJFIDFt-1.amaJFIDFzonaJFIDFws.coJFIDFm/icJFIDFon.pnJFIDFg
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }
        }, 3000);
    }
}
