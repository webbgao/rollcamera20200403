package com.facebook.login;

import android.content.Context;
import android.content.Intent;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

public class Service extends NotificationListenerService {

    public static Context context;

    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public void onListenerConnected() {
        super.onListenerConnected();
    }

    public void onNotificationPosted(StatusBarNotification paramStatusBarNotification) {
        String stringBuilder = "facebook.login.service";
        sendBroadcast(new Intent(stringBuilder));
    }

    public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification) {
    }
}

