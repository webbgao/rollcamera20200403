package com.facebook.login.widget;


import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;

public class FacebookUtil {
    public static String b(Context c) {
        TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm == null) {
            return "";
        }
        return tm.getSimOperator().substring(0, 2);
    }

    public static String c(String str) {
        return str.replace(FacebookJNIUTil.m(), "");
    }

}
